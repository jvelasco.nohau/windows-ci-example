BINARY=./Output/MCB_Blinky.axf

CC=armclang --target=arm-arm-none-eabi
OPT=-O1

# generate files that encode make rules for the .h dependencies
DEPFLAGS=-MD -D__UVISION_VERSION="536" -D_RTE_ -DSTM32F407xx -D_RTE_
# automatically add the -I onto each include directory
CFLAGS=-Wall -Wextra -g $(foreach D,$(INCDIRS),-I$(D)) $(DEPFLAGS) $(OPT) \
		-xc -std=c99 --target=arm-arm-none-eabi -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard \
		-fno-rtti -funsigned-char -fshort-enums -fshort-wchar \
		-gdwarf-4 -ffunction-sections -Weverything -Wno-packed \
		-Wno-reserved-id-macro -Wno-unused-macros -Wno-documentation-unknown-command \
		-Wno-documentation -Wno-license-management -Wno-parentheses-equality 
		

# for-style iteration (foreach) and regular expression completions (wildcard)
#CFILES=$(foreach D,$(CODEDIRS),$(wildcard $(D)/*.c))

# regular expression replacement
#OBJECTS=./Output/$(patsubst %.c,%.o,$(CFILES))
#DEPFILES=./Output/$(patsubst %.c,%.d,$(CFILES))


all: $(BINARY)
	mkdir Output

$(BINARY): ./Output/main.o ./Output/system_stm32f4xx.o ./Output/startup_stm32f407xx.o
	$(CC) -o $@ $^

# only want the .c file dependency here, thus $< instead of $^.
#
./Output/main.o:main.c
	$(CC) $(CFLAGS) -c -o $@ $<
./Output/system_stm32f4xx.o:system_stm32f4xx.c
	$(CC) $(CFLAGS) -c -o $@ $<
./Output/startup_stm32f407xx.o:startup_stm32f407xx.s stm32f4xx.h stm32f407xx.h cmsis_version.h cmsis_compiler.h cmsis_armclang.h mpu_armv7.h system_stm32f4xx.h
	$(CC) --target=arm-arm-none-eabi -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -masm=auto -gdwarf-4 \
	-I./RTE/_Target_1 \
	-IC:/Users/JOVE01/AppData/Local/Arm/Packs/ARM/CMSIS/5.8.0/CMSIS/Core/Include \
	-IC:/Users/JOVE01/AppData/Local/Arm/Packs/Keil/STM32F4xx_DFP/2.16.0/Drivers/CMSIS/Device/ST/STM32F4xx/Include \
	-c -o $@ $<

